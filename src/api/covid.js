import axios from "axios";

export const loadCovidData=(country)=>{
    return axios.get(`https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/stats?country=${country}`,
        {
            headers: {
                'x-rapidapi-host': 'covid-19-coronavirus-statistics.p.rapidapi.com', //oauth
                'x-rapidapi-key': '4557b7ca6cmsh0b81bd8a7bfbfcap15077fjsn3b9682f62ec1',
            },
        });
}