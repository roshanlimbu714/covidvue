import Vue from 'vue'
import Vuex from 'vuex'
import {loadCovidData} from "../api/covid";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        count: 100,
        covidStats:[]
    },
    mutations: {
        setCount(state, {value}) {
            console.log(value);
            state.count += value;
        },
        setCovidStats(state,data){
            console.log(data);
            state.covidStats=[...data];
            console.log(state.covidStats);
        }
    },
    actions: {
        increaseCount({commit}, data) {
            console.log(data);
            commit('setCount', {value:data})
        },
        async loadCovidStats({commit}, country){
            let res= await loadCovidData(country);
            console.log(res.data.data.covid19Stats);
            commit('setCovidStats',res.data.data.covid19Stats)
        }
    },
    modules: {}
})
